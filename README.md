# A propos du projet
Il s'agit d'un projet de conception en Genie Logiciel realiser a l'universite de Dschang par des\
etudiants d'informatique 4, sous la supervision de Dr Soh dont le but est de concevoir une plateforme\
de comparaison des centres de formation. Pour mener a bien ce projet, nous avons produit un document\
de Genie Logiciel qui relate les etapes de realisation du dit projet, puis une implementation bien que\
non aboutie impliquant les technologies ReactJS, FireBase, ...\

## Ce qui a ete fait
Dans ce projet, nous implementons une interface pour la page d'accueil, un CRUD de connexion avec firebase\
une liste de quelques centres avec des informations trouves sur le net (ILs s'agit de centre dans le domaine anglophone)\
une option favoris pour les ecoles que l'on souhaite revisiter ou qu'on a aimer. Cela servira pour des sondages\
ulterieure. Si l'on a pris des ecoles anglophones c'est parce qu'elles ont des plateformes en ligne...\
un blog pour le forum a egalement ete implementer.
La base de donnees du projet se trouve en ligne sous firebase avec un acces prive\

# Comment recuperer et configurer le projet?

## Projet React

Ce projet a ete realise avec [Create React App](https://github.com/facebook/create-react-app).

## Script disponible
Depuis la racine du projet vous pouvez taper les commande suivantes:

### `npm start`

Lancer l'application en mode developpement.\
Ouvrir [http://localhost:3000](http://localhost:3000) pour le rendu dans le navigateur.

Les modifications feront automatiquement recharger la page.\
Les erreurs apparaitrons dans la console de votre navigateur.

### `npm run build`

compile les scripts React pour produire des fichiers JS dans le repertoire `build`.\
