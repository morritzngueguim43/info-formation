import "./App.css";
import Navbar from "./Components/Navbar";
import { Route, Routes } from "react-router-dom";
import Home from "./Pages/Home";
import Favourites from "./Pages/Favourites";
import Feedback from "./Pages/FeedbackUI";
import Login from "./Pages/UserAccount/Login";
import SignUp from "./Pages/UserAccount/SignUp";
import Reset from "./Pages/UserAccount/ResetPassword";
import Dashboard from "./Pages/UserAccount/Dashboard";
import Error from "./Pages/Error";

import Forum from "./Pages/Forum/ForumUI";
import PostCreate from "./Pages/Forum/CreatePost";
import Post from "./Pages/Forum/PostUI";

import ChangePassword from "./Pages/UserAccount/ChangePassword";

import Compare from "./Pages/Compare";
import ErrorCompare from "./Pages/ErrorCompare";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="home" element={<Home />} />
        <Route path="favourites" element={<Favourites />} />
        <Route path="forum" element={<Forum />} />
        <Route path="compare" element={<Compare />} />
        <Route>
          {""}
          <Route path="forum/CreatePost" element={<PostCreate />} />

          <Route path="forum/Post/:title/:postId" element={<Post />} />
        </Route>
        <Route path="feedback" element={<Feedback />} />
        <Route path="login" element={<Login />} />
        <Route path="signup" element={<SignUp />} />
        <Route path="changePassword" element={<ChangePassword />} />
        <Route exact path="/reset" element={<Reset />} />
        <Route path="dashboard" element={<Dashboard />} />
        <Route path="*" element={<Error />} />
        <Route path="*" element={<ErrorCompare />} />
      </Routes>
    </>
  );
}

export default App;
                                                                                                              /*   Morritz   */