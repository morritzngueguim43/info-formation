import React from "react";
import { useContext } from "react";
import CompareContext from "../Contexts/CompareContext";
import SchoolsList2 from "../Components/SchoolsList2";
import "../PagesCSS/Compare.css";
import { Link } from "react-router-dom";

function Compare() {
  const compareCtx = useContext(CompareContext);
  
  let content;

  if (compareCtx.totalSchools === 0) {
    content = (
      <div className="comp-empty">
        <h2 className="comp-empty-heading">Ps de centres a compare!</h2>
        <p className="comp-empty-text">
          Voulez vous connaitre le centre qui est fait pour vous?
          <br></br>
          Comparer des centres maintenant!
        </p>
        <Link to="/schools">
          <button className="fav-button">Demarrer l'exploration</button>
        </Link>
      </div>
    );
  } else {
    content = (
      <div className="compare-school">
        {compareCtx.school.map((prop) => <SchoolsList2 key={prop._id} schools={prop} />)}

      </div>
    );
  }

  return (
    <div className="comp-page">
      <div className="comp-header">
        <h1 className="comp-title">
          Centre a comparer ({compareCtx.totalSchools})
        </h1>
      </div>
      {content}
    </div>
  );
}

export default Compare;

                                                                                                              /*   Morritz   */