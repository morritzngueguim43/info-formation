import React from "react";
import { useContext } from "react";
import FavouritesContext from "../Contexts/FavouritesContext";
import SchoolsList from "../Components/SchoolsList";
import "../PagesCSS/Favourites/Favourites.css";
import { Link } from "react-router-dom";

import pic from "../PagesCSS/Favourites/heart.png";

function Favourites() {
  const favouritesCtx = useContext(FavouritesContext);
  // recuperer le contexte actuel des favoris

  let content;

  if (favouritesCtx.totalFavourites === 0) {
    content = (
      <div className="fav-empty">
        <img src={pic} alt="Heart" className="fav-img" />
        <h2 className="fav-empty-heading">Pas encore de favoris!</h2>
        <p className="fav-empty-text">
          Voir un centre que vous aimez?
          <br></br>
          Sauvegarder en tant que favoris!
        </p>
        <Link to="/schools">
          <button className="fav-button">Demarrer l'exploration</button>
        </Link>
      </div>
    );
  } else {
    content = (
      <div>
        <SchoolsList schools={favouritesCtx.favourites} />
      </div>
    );
  }

  return (
    <section>
      <div className="fav-header">
        <h1 className="fav-title">
          Les centres qui m'interesse ({favouritesCtx.totalFavourites})
        </h1>
      </div>
      {content}
    </section>
  );
}

export default Favourites;

                                                                                                              /*   Morritz   */