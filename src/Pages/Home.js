import React from "react";
import { Link } from "react-router-dom";
import "../PagesCSS/Home/Home.css";
import pic1 from "../PagesCSS/Home/choose.png";
import pic2 from "../PagesCSS/Home/search.png";

function Home() {
  return (
    <section>
      <div className="home-section1">
        <h1 className="home-heading1">
          Vous cherchez<br></br> Le meilleur centre formation?
        </h1>
        <p className="home-description1">
          Avec <b><i>Info-Formation</i></b>, Vous pouvez choisir la meilleure<br></br> formation pour vous, ou autre 
          en quelques <i>clicks</i>!
        </p>
        <img src={pic1} alt="Choose" className="home-img1" />
      </div>
      <div className="home-section2">
        <div>
          <h2 className="home-heading2">
            Toutes les cartes don't vous avez besoins en quelques <i>clicks</i>.
          </h2>
          <p className="home-description2">
            Visitez les differentes formations{" "}
            <Link to="/schools" className="home-link2">
              <b>Formation</b>
            </Link>{" "}
            Et ajouter des formations a vos{" "}
            <Link to="/favourites" className="home-link2">
              <b>favoris</b>
            </Link>{" "}
            pour les referencement futur!
          </p>
        </div>
        <div className="home-img2">
          <img src={pic2} alt="Search" className="home-img2" />
        </div>
      </div>
      <div className="home-section3">
        <h2 className="home-heading3">Qu'attendez vous?</h2>
        <Link to="/SignUp">
          <button className="home-button"><i>Inscrivez-vous</i></button>
        </Link>
        <p className="home-description3">
          ou{" "}
          <Link to="/schools" className="home-link3">
            <i>Demarrer l'exploration</i>
          </Link>{" "}
          avec un compte.
        </p>
      </div>
    </section>
  );
}

export default Home;

                                                                                                              /*   Morritz   */