import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { auth, logInWithEmailAndPassword } from "../../Firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import "../../PagesCSS/Login.css";
import BackgroundParticle from "../../Components/BackgroundParticle";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, loading /*, error */] = useAuthState(auth);
  const navigate = useNavigate();
  useEffect(() => {
    if (loading) {
      return;
    }
    if (user) navigate("/");
  }, [user, loading, navigate]);

  return (
    <div className="login">
      <BackgroundParticle />
      <div className="login__container">
        <input
          type="text"
          className="login__textBox"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Adresse email"
        />
        <input
          type="password"
          className="login__textBox"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Mot de passe"
        />
        <button
          className="login__btn"
          onClick={() => logInWithEmailAndPassword(email, password)}
        >
          Connexion
        </button>
        <div>
          <Link to="/reset" className="forget__password">
            Mot de passe Oublier?
          </Link>
        </div>
        <div>
          Vous n'avez pas de compte?{" "}
          <Link to="/SignUp" className="sign__up">
            S'enregistrer
          </Link>{" "}
          maintenant.
        </div>
      </div>
    </div>
  );
}
export default Login;

                                                                                                              /*   Morritz   */