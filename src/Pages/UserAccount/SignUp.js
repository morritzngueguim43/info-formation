import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useNavigate } from "react-router-dom";
import { auth, registerWithEmailAndPassword } from "../../Firebase";
import { sendEmailVerification } from "firebase/auth";
import "../../PagesCSS/SignUp.css";
import BackgroundParticle from "../../Components/BackgroundParticle";
import { toast } from "react-toastify";

function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [user, loading /*, error*/] = useAuthState(auth);
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const register = () => {
    if (!name || name.replace(/\s/g, "") === "") alert("Entrer votre nom");
    else registerWithEmailAndPassword(name, email, password, setIsLoading);
  };
  useEffect(() => {
    if (loading || isLoading) return;
    if (user) {
      navigate("/dashboard", { replace: true });
      sendEmailVerification(user).then(() => {
        toast(
          "Un mail a ete envoyee. Veuillez consulter celui-ci pour continuer.",
          { type: "success" }
        );
      });
    }
  }, [user, loading, isLoading, navigate]);

  return (
    <div className="register">
      <BackgroundParticle />
      <div className="register__container">
        <input
          type="text"
          className="register__textBox"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Nom en entier"
        />
        <input
          type="text"
          className="register__textBox"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Adresse email"
        />
        <input
          type="password"
          className="register__textBox"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Mot de passe"
        />
        <button
          className="register__btn"
          onClick={register}
          disabled={isLoading || loading}
        >
          S'enregistrer
        </button>
        <div>
          Vous avez deja un compte?{" "}
          <Link to="/login" className="login__now">
            connecter-vous
          </Link>{" "}
          maintenant.
        </div>
      </div>
    </div>
  );
}
export default Register;

                                                                                                              /*   Morritz   */