import React from "react";
import { createContext } from "react";
import { useState, useEffect } from "react";

const FavouritesContext = createContext({
  favourites: [],
  totalFavourites: 0,
  addFavourite: (favouriteSchool) => {},
  removeFavourite: (schoolId) => {},
  itemIsFavourite: (schoolId) => {},
}); //context est un objet javascript

export function FavouritesContextProvider(props) {
  const [userFavourites, setuserFavourites] = useState(() => {
    const saved = localStorage.getItem("favourites");
    const initialValue = JSON.parse(saved);
    return initialValue || [];
  });

  function addFavouriteHandler(favouriteSchool) {
    setuserFavourites((prevUserFavourites) => {
      return prevUserFavourites.concat(favouriteSchool);
    });
  }

  function removeFavouriteHandler(schoolId) {
    setuserFavourites((prevUserFavourites) => {
      return prevUserFavourites.filter((school) => school._id !== schoolId);
    });
  }

  function itemIsFavouriteHandler(schoolId) {
    return userFavourites.some((school) => school._id === schoolId);
  }

  const context = {
    favourites: userFavourites,
    totalFavourites: userFavourites.length,
    addFavourite: addFavouriteHandler,
    removeFavourite: removeFavouriteHandler,
    itemIsFavourite: itemIsFavouriteHandler,
  };

  useEffect(() => {
    localStorage.setItem("favourites", JSON.stringify(context.favourites));
  }, [context.favourites]);

  return (
    <FavouritesContext.Provider value={context}>
      {props.children}
    </FavouritesContext.Provider>
  );
}

export default FavouritesContext;
