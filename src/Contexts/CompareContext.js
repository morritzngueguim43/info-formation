import React from "react";
import { createContext } from "react";
import { useState } from "react";

const CompareContext = createContext({
  school: [],
  totalSchools: 0,
  addToCompare: (compareSchool) => {},
  removeFromCompare: (schoolId) => {},
  itemToCompare: (schoolId) => {},
}); 
export function CompareContextProvider(props) {
  const [schoolsToCompare, setSchoolsToCompare] = useState([]);

  function addCompareHandler(compareSchool) {
    setSchoolsToCompare((prevSchools) => {
      return prevSchools.concat(compareSchool);
    });
  }

  function removeCompareHandler(schoolId) {
    setSchoolsToCompare((prevSchools) => {
      return prevSchools.filter((school) => school._id !== schoolId);
    });
  }

  function itemCompareHandler(schoolId) {
    return schoolsToCompare.some((school) => school._id === schoolId);
  }

  const context = {
    school: schoolsToCompare,
    totalSchools: schoolsToCompare.length,
    addToCompare: addCompareHandler,
    removeFromCompare: removeCompareHandler,
    itemToCompare: itemCompareHandler,
  };

  return (
    <CompareContext.Provider value={context}>
      {props.children}
    </CompareContext.Provider>
  );
}

export default CompareContext;
