import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { FavouritesContextProvider } from "./Contexts/FavouritesContext";
import "react-toastify/dist/ReactToastify.css";
import "react-bootstrap";
import { ToastContainer } from "react-toastify";
import { SchoolsContextProvider } from "./Contexts/SchoolsContext";
import { CompareContextProvider } from "./Contexts/CompareContext";

ReactDOM.render(
  <SchoolsContextProvider>
    <FavouritesContextProvider>
      <CompareContextProvider>
      <BrowserRouter>
        <App />
        <ToastContainer />
      </BrowserRouter>
      </CompareContextProvider>
    </FavouritesContextProvider>
  </SchoolsContextProvider>,
  document.getElementById("root")
);
/**
 * Point d'entrer de notre application, c'est ici que tous les script react seront charges
 */